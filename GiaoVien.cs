﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    class GiaoVien : NguoiLaoDong
    {
        private double heSoLuong;

        public GiaoVien()
        {
        }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
            : base(hoTen, namSinh, luongCoBan)
        {
            this.heSoLuong = heSoLuong;
        }

        public void NhapThongTin(double heSoLuong)
        {
            this.heSoLuong = heSoLuong;
        }

        public override double TinhLuong()
        {
            //double luongCoBan = layLuongCoBan(); //Lay tu phuong thuc cua lop cha
            //return luongCoBan * heSoLuong * 1.25;
            return base.layLuongCoBan() * heSoLuong * 1.25;
        }

        public override void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine($"He so luong: {heSoLuong}, Luong: {TinhLuong()}");
        }

        public double XuLy()
        {
            return heSoLuong + 0.6;
        }
    }
}
