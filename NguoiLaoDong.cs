﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    class NguoiLaoDong
    {
        private string hoTen { get; set; }
        private int namSinh { get; set; }
        private double luongCoBan { get; set; }

        public NguoiLaoDong()
        {
        }

        public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
        {
            this.hoTen = hoTen;
            this.namSinh = namSinh;
            this.luongCoBan = luongCoBan;
        }

        protected double layLuongCoBan()
        {
            return luongCoBan;
        }
        public void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
        {
            this.hoTen = hoTen;
            this.namSinh = namSinh;
            this.luongCoBan = luongCoBan;
        }

        public virtual double TinhLuong()
        {
            return luongCoBan;
        }

        public virtual void XuatThongTin()
        {
            Console.WriteLine($"Ho ten la: {hoTen}, nam sinh: {namSinh}, luong co ban: {luongCoBan}.");
        }
        
    }
}
