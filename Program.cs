﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Assignment1
{
    class Program
    {
        static bool KiemTraNamSinh(int namSinh)
        {
            return namSinh >= 1900 && namSinh <= 2050;
        }

        static void Main()
        {
            Console.Write("Hay nhap vao so luong giao vien: ");
            int n;

            //Kiem tra phai nhap vao giao vien
            while (!int.TryParse(Console.ReadLine(), out n) || n <= 0)
            {
                Console.WriteLine("So luong giao vien phai la so duong. Xin vui long nhap lai:");
                Console.Write("Hay nhap vao so luong giao vien: ");
            }

            List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

            string hoTen;
            int namSinh = 0;
            double luongCoBan = 0;
            double heSoLuong = 0;

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Nhap thong tin cho giao vien {i + 1}:");

                bool nhapLai;

                do
                {
                    nhapLai = false;

                    Console.Write("Ho ten: ");
                    hoTen = Console.ReadLine();

                    // Kiem tra ho ten
                    if (!Regex.IsMatch(hoTen, "^[a-zA-Z ]+$"))
                    {
                        Console.WriteLine("Ho ten phai la chu cai, khong phai la so!!");
                        nhapLai = true;
                        continue;
                    }

                    Console.Write("Nam sinh: ");
                    if (!int.TryParse(Console.ReadLine(), out namSinh) || !KiemTraNamSinh(namSinh))
                    {
                        Console.WriteLine("Nam sinh tu 1900-2050");
                        nhapLai = true;
                        continue;
                    }

                    Console.Write("Luong co ban: ");
                    if (!double.TryParse(Console.ReadLine(), out luongCoBan) || luongCoBan < 0)
                    {
                        Console.WriteLine("luong co ban phai la so duong.");
                        nhapLai = true;
                        continue;
                    }

                    Console.Write("He so luong: ");
                    if (!double.TryParse(Console.ReadLine(), out heSoLuong) || heSoLuong < 0)
                    {
                        Console.WriteLine("He so luong phai la so duong.");
                        nhapLai = true;
                    }

                } while (nhapLai);

                GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
                danhSachGiaoVien.Add(giaoVien);
            }

            if (danhSachGiaoVien.Count > 0)
            {
                Console.WriteLine("\nDanh sach giao vien:");
                foreach (var giaoVien in danhSachGiaoVien)
                {
                    giaoVien.XuatThongTin();
                    Console.WriteLine();
                }

                double luongThapNhat = danhSachGiaoVien[0].TinhLuong();
                GiaoVien giaoVienLuongThapNhat = danhSachGiaoVien[0];

                foreach (var giaoVien in danhSachGiaoVien)
                {
                    if (giaoVien.TinhLuong() < luongThapNhat)
                    {
                        luongThapNhat = giaoVien.TinhLuong();
                        giaoVienLuongThapNhat = giaoVien;
                    }
                }

                Console.WriteLine("\nThong tin cua giao vien co luong thap nhat:");
                foreach (var giaoVien in danhSachGiaoVien)
                {
                    if (giaoVien.TinhLuong() == luongThapNhat)
                    {
                        giaoVien.XuatThongTin();
                    }
                }
            }
            else
            {
                Console.WriteLine("Danh sach giao vien rong. Khong co giao vien de xuat thong tin.");
            }
            Console.ReadKey();
        }
    }
}
